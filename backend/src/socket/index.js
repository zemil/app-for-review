import socketIO from 'socket.io';

import {
    CHANGE_USERS_IN_SEARCH,
    START_GAME,
    CONNECT_USER,
    GIVE_WIN,
    FINISH_GAME
} from './events';
import getValidPairIndex from '../utils/getValidPairIndex';

const usersInSearch = [];
const usersInGame = [];

function initSockets(server) {
    const io = socketIO.listen(server);

    io.on('connection', socket => {
        console.log('[connect] usersInSearch: ', usersInSearch);

        socket.on(CONNECT_USER, userData => {
            const { username } = userData;

            // to block the same user
            // e.g.: user can open multiple tabs in browser
            const isNewUser = !usersInSearch.some(
                connectedUser => connectedUser.username === username
            );

            console.log(`Is ${username} New? --> ${isNewUser}`);

            if (isNewUser) {
                userData.socketId = socket.id;

                usersInSearch.push(userData);

                io.sockets.emit(CHANGE_USERS_IN_SEARCH, usersInSearch);

                // find valid user for the first user
                const user2Index = getValidPairIndex(usersInSearch);

                // connect pair to single room:
                if (user2Index) {
                    const user1 = usersInSearch[0];
                    const user2 = usersInSearch[user2Index];

                    // TODO: maybe it needs to encode a roomId
                    const roomId = `${user1.username}vs${user2.username}`;

                    // send roomId to each users:
                    io.to(user1.socketId).emit(START_GAME, {
                        opponent: user2,
                        roomId
                    });
                    io.to(user2.socketId).emit(START_GAME, {
                        opponent: user1,
                        roomId
                    });

                    // and then delete these users from usersInSearch
                    usersInSearch.shift();
                    usersInSearch.splice(user2Index, 1);
                    io.sockets.emit(CHANGE_USERS_IN_SEARCH, usersInSearch);
                    // push the pair to game list
                    usersInGame.push(user1, user2);
                }
            }
        });

        socket.on(GIVE_WIN, winData => {
            const { winner, loser } = winData;
            const finishGameData = {
                winner,
                loser
            };

            io.sockets.emit(FINISH_GAME, finishGameData);
        });

        socket.on('disconnect', () => {
            const userIndex = usersInSearch.findIndex(
                user => user.socketId === socket.id
            );

            if (userIndex > -1) {
                console.log('[disconnect] usersInSearch: ', userIndex);
                usersInSearch.splice(userIndex, 1);
                io.sockets.emit(CHANGE_USERS_IN_SEARCH, usersInSearch);
            }
        });
    });
}

export default initSockets;
