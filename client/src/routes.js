import React from 'react';
import { Redirect } from 'react-router-dom';

import LoginPage from './pages/Auth/Login';
import SignUpPage from './pages/Auth/SignUp';
import AboutPage from './pages/About';
import MirrorPage from './pages/Mirror';
import GamePage from './pages/GameStepper';
import ResetPasswordPage from './pages/Auth/ResetPassword';
import NewPasswordPage from './pages/Auth/NewPassword';
import NotFoundPage from './pages/NotFound';
import TestPage from './pages/Test';

const routes = [
	{
		path: '/',
		exact: true,
		component: props => (
			<Redirect
				to={{
					pathname: '/login',
					state: { from: props.location },
				}}
			/>
		),
	},
	// public page for testing
	{
		path: '/test',
		component: props => <TestPage {...props} />,
	},
	{
		path: '/login',
		component: props => <LoginPage {...props} />,
	},
	{
		path: '/sign-up',
		component: props => <SignUpPage {...props} />,
	},
	{
		path: '/reset-password',
		component: props => <ResetPasswordPage {...props} />,
	},
	{
		path: '/about',
		private: true,
		component: props => <AboutPage {...props} />,
	},
	{
		path: '/mirror',
		private: true,
		component: props => <MirrorPage {...props} />,
	},
	{
		path: '/new-password',
		private: true,
		component: props => <NewPasswordPage {...props} />,
	},
	{
		path: '/game',
		private: true,
		component: props => <GamePage {...props} />,
	},
	{
		path: '*',
		component: props => <NotFoundPage {...props} />,
	},
];

export default routes;
