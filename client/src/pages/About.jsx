import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from '@emotion/styled';
import DeleteIcon from '@material-ui/icons/ArrowForward';
import { FormControlLabel, FormControl, Radio, RadioGroup } from '@material-ui/core';

import { changeUserField, saveUserData } from '../modules/user/userActions';

import Container from '../components/UI/CenteredContainer';
import TextField from '../components/UI/TextField';
import Fab from '../components/UI/Fab';

const Title = styled.h2`
	font-size: 30px;
	text-align: center;
`;

const Form = styled.form`
	max-width: 320px;
	width: 100%;
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: center;

	& > span,
	button {
		margin-bottom: 25px;
	}

	& > span > input {
		width: 100%;
	}

	.p-calendar {
		width: 100%;
	}

	.gender-select {
		flex-direction: row;
		justify-content: center;
	}

	.about-submit {
		margin: 30px;
	}
`;

const InlineInput = styled.div`
	display: flex;
	justify-content: space-between;

	& > div:first-child {
		margin-right: 10px;
	}
`;

// const GENDERS = [{ label: 'male', value: 'male' }, { label: 'female', value: 'female' }];

class AboutPage extends Component {
	constructor(props) {
		super(props);

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmitData = this.handleSubmitData.bind(this);
	}

	handleChange(e) {
		const { value, name } = e.target;
		const { changeUserField } = this.props;

		changeUserField(name, value);
	}

	handleSubmitData(e) {
		e.preventDefault();

		this.props.saveUserData();

		// TODO: need validation
		// better replace it to action with validation:
		this.props.history.push('/mirror');
	}

	render() {
		const {
			user: { name, birthday, gender, opponentGender, opponentAgeFrom, opponentAgeTo },
		} = this.props;

		return (
			<Container>
				<Form onSubmit={this.handleSubmitData}>
					<Title>About me:</Title>

					<TextField
						error={false}
						label="Name"
						name="name"
						margin="normal"
						variant="outlined"
						value={name}
						onChange={this.handleChange}
					/>

					<TextField
						// error
						id="date"
						label="Birthday"
						type="date"
						name="birthday"
						margin="normal"
						onChange={this.handleChange}
						value={birthday}
						InputLabelProps={{
							shrink: true,
						}}
					/>

					<FormControl component="fieldset">
						<RadioGroup
							aria-label="Gender"
							name="gender"
							value={gender}
							className="gender-select"
							onChange={this.handleChange}
						>
							<FormControlLabel value="female" control={<Radio />} label="Female" />
							<FormControlLabel value="male" control={<Radio />} label="Male" />
						</RadioGroup>
					</FormControl>

					<Title>I'm looking for:</Title>

					<FormControl component="fieldset">
						<RadioGroup
							aria-label="Gender"
							name="opponentGender"
							value={opponentGender}
							className="gender-select"
							onChange={this.handleChange}
						>
							<FormControlLabel value="female" control={<Radio />} label="Female" />
							<FormControlLabel value="male" control={<Radio />} label="Male" />
							<FormControlLabel value="both" control={<Radio />} label="Both" />
						</RadioGroup>
					</FormControl>

					<InlineInput>
						<TextField
							id="outlined-number"
							label="Age from"
							name="opponentAgeFrom"
							value={opponentAgeFrom}
							onChange={this.handleChange}
							type="number"
							InputLabelProps={{
								shrink: true,
							}}
							margin="normal"
							variant="outlined"
						/>
						<TextField
							id="outlined-number"
							label="Age to"
							name="opponentAgeTo"
							value={opponentAgeTo}
							onChange={this.handleChange}
							type="number"
							InputLabelProps={{
								shrink: true,
							}}
							margin="normal"
							variant="outlined"
						/>
					</InlineInput>

					<Fab variant="extended" color="primary" className="about-submit" type="submit">
						Next
						<DeleteIcon />
					</Fab>
				</Form>
			</Container>
		);
	}
}

const mapStateToProps = ({ user }) => ({ user });

export default connect(
	mapStateToProps,
	{ changeUserField, saveUserData }
)(withRouter(AboutPage));
