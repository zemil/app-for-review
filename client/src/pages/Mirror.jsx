import React, { Fragment } from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import ReadyIco from '@material-ui/icons/Done';
import ProfileIco from '@material-ui/icons/AccountCircle';

import Mirror from '../components/Mirror';
import BottomBar from '../components/UI/BottomBar';
import Fab from '../components/UI/Fab';

const MirrorBottomBar = styled(BottomBar)`
	position: fixed;
	left: 0;
	bottom: 50px;
	width: 100%;
	z-index: 999;
	text-align: center;

	a {
		margin: 0 10px;
	}
`;

class MirrorPage extends React.Component {
	render() {
		return (
			<Fragment>
				<Mirror />

				<MirrorBottomBar>
					<Fab variant="extended" size="large" component={Link} to="/about">
						Profile
						<ProfileIco />
					</Fab>

					<Fab
						variant="extended"
						color="primary"
						size="large"
						component={Link}
						to="/game"
					>
						I'm ready
						<ReadyIco />
					</Fab>
				</MirrorBottomBar>
			</Fragment>
		);
	}
}

export default MirrorPage;
