import React, { Component } from 'react';
import styled from '@emotion/styled';
import { FormControlLabel, FormControl, FormHelperText, Checkbox } from '@material-ui/core';
import { Link } from 'react-router-dom';

import api from '../../utils/api';
import helpers from '../../utils/helpers';

import Fab from '../../components/UI/Fab';
import TextField from '../../components/UI/TextField';

import AuthLayout from './Layout';

const LoginBlock = styled.div`
	text-align: center;

	a {
		color: #909090;
		text-decoration: underline;
	}
`;

class Signup extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: '',
			email: '',
			agreement: false,
			invalidFields: [],
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmitSignUp = this.handleSubmitSignUp.bind(this);
	}

	handleChange(e) {
		const { name, value, checked } = e.target;

		this.setState({
			[name]: name === 'agreement' ? checked : value,
		});
	}

	get isValidForm() {
		const { username, email, password, agreement } = this.state;
		const invalidFields = [];

		if (username.trim().length < 4) {
			invalidFields.push('username');
		}

		if (!helpers.isEmail(email)) {
			invalidFields.push('email');
		}

		if (password.length < 8) {
			invalidFields.push('password');
		}

		if (!agreement) {
			invalidFields.push('agreement');
		}

		this.setState({
			invalidFields,
		});

		return invalidFields.length === 0;
	}

	handleSubmitSignUp(e) {
		e.preventDefault();

		const { username, email, password } = this.state;

		if (this.isValidForm) {
			const data = {
				username,
				email,
				password,
			};

			api.post('sign-up', data)
				.then(data => data.json())
				.then(json => {
					if (json.success) {
						alert('You can log in with your data');
					} else {
						alert('Something went wrong');
					}
				});
		}
	}

	render() {
		const { username, email, password, agreement, invalidFields } = this.state;

		return (
			<AuthLayout onSubmit={this.handleSubmitSignUp}>
				<TextField
					error={invalidFields.includes('username')}
					label="Username"
					type="text"
					name="username"
					margin="normal"
					variant="outlined"
					value={username}
					onChange={this.handleChange}
				/>

				<TextField
					error={invalidFields.includes('email')}
					label="Email"
					type="email"
					name="email"
					margin="normal"
					variant="outlined"
					value={email}
					onChange={this.handleChange}
				/>

				<TextField
					error={invalidFields.includes('password')}
					label="Password"
					type="password"
					autoComplete="current-password"
					name="password"
					margin="normal"
					variant="outlined"
					value={password}
					onChange={this.handleChange}
				/>

				<FormControl required error={invalidFields.includes('agreement')}>
					<FormControlLabel
						control={
							<Checkbox
								checked={agreement}
								name="agreement"
								onChange={this.handleChange}
								color="primary"
							/>
						}
						label={
							<span>
								I accept the <a href="#agreement">Terms And Conditions</a>
							</span>
						}
					/>
					{invalidFields.includes('agreement') && (
						<FormHelperText>You should accept the terms</FormHelperText>
					)}
				</FormControl>

				<Fab variant="extended" color="primary" type="submit">
					Sign up
				</Fab>

				<LoginBlock>
					Already have an account? <Link to="/login">Log in</Link>
				</LoginBlock>
			</AuthLayout>
		);
	}
}

export default Signup;
