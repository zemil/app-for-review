import React from 'react';

import FullScreen from './FullScreen';
import Loader from '../UI/Loader';
import BluredWrapper from './BluredWrapper';

import logo from '../../media/images/logo-big.png';

export default function PreloaderScreen() {
	return (
		<FullScreen>
			<BluredWrapper />

			<Loader inner={<img src={logo} alt="Spark2Date" />} />
		</FullScreen>
	);
}
