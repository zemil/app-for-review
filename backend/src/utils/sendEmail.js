import nodemailer from 'nodemailer';

import {
    SPARK_SERVISE_EMAIL,
    SPARK_SERVISE_EMAIL_PASSWORD
} from '../utils/constants';

async function sendEmail(to, subject, html) {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: SPARK_SERVISE_EMAIL,
            pass: SPARK_SERVISE_EMAIL_PASSWORD
        }
    });

    const mailOptions = {
        from: SPARK_SERVISE_EMAIL,
        to,
        subject,
        html
    };

    let response = await transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            console.log(err);
        } else {
            console.log(info);
        }
    });

    return response;
}

export default sendEmail;
