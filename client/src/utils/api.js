import { API_URL } from './constants';
import ll from './localStorage';

const apiPath = path => `${API_URL}/api/${path}`;

async function request(method, path, data) {
	const params = [apiPath(path)];

	let options;
	if (['POST', 'PATCH'].includes(method)) {
		options = {
			method,
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': ll.get('usertoken'),
			},
			body: JSON.stringify(data),
			credentials: 'omit',
		};
	} else {
		options = {
			headers: {
				'x-access-token': ll.get('usertoken'),
			},
		};
	}

	params.push(options);

	try {
		const response = await fetch(...params);
		const status = response.status;

		if (status === 401) {
			ll.remove('usertoken');

			window.location = '/login';
		}

		return response;
	} catch (error) {
		console.error('api error', error);
	}
}

const api = {
	get(path) {
		return request('GET', path);
	},

	post(path, data) {
		return request('POST', path, data);
	},

	patch(path, data) {
		return request('PATCH', path, data);
	},
};

export default api;
