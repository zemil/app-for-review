import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

import User from '../../schemes/user';
import { JWT_SECRET } from '../../utils/constants';

// TODO: should use passport.js for more security
function loginUser(req, res, next) {
    const { username, password } = req.body;
    const searchKey = username.includes('@') ? 'email' : 'username';

    User.findOne(
        {
            [searchKey]: username
        },
        (err, user) => {
            if (err) throw err;

            if (!user) {
                res.json({
                    success: false,
                    message: 'Authentication failed. User not found.'
                });
            } else if (user) {
                bcrypt.hash(password, user.magic, function(hashError, hash) {
                    if (hashError) {
                        return next(hashError);
                    }

                    // replace a password string with hash value
                    // check if password matches
                    if (user.password != hash) {
                        res.json({
                            success: false,
                            message: 'Authentication failed. Wrong password.'
                        });
                    } else {
                        // data to decode:
                        const payload = {
                            userId: user.id
                        };

                        const token = jwt.sign(payload, JWT_SECRET, {
                            expiresIn: '7d'
                        });

                        // return the information including token as JSON
                        res.json({
                            success: true,
                            message: 'Enjoy your token!',
                            token: token
                        });
                    }
                });
            }
        }
    );
}

export default loginUser;
