import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import * as SWRTC from '@andyet/simplewebrtc';

import { CONFIG_URL } from '../utils/constants';

import routes from '../routes';

function SmartRoute({ isAuthenticated, ...props }) {
	const { private: privateRoute } = props;

	if (!privateRoute && isAuthenticated) {
		return <Redirect to="/mirror" />;
	}

	return <Route {...props} />;
}

class Root extends React.Component {
	render() {
		const { isAuthenticated } = this.props;

		return (
			<SWRTC.Provider configUrl={CONFIG_URL}>
				<Router>
					<Switch>
						{routes.map(route => {
							const isPrivateRoute = route.private;

							if (isPrivateRoute && !isAuthenticated) {
								return <Redirect key="protected" to="/login" />;
							}

							return (
								<SmartRoute
									key={route.path}
									isAuthenticated={isAuthenticated}
									{...route}
								/>
							);
						})}
					</Switch>
				</Router>
			</SWRTC.Provider>
		);
	}
}

const mapStateToProps = ({ user }) => ({ isAuthenticated: user.isAuthenticated });

export default connect(mapStateToProps)(Root);
