# Server side

-   development: `npm run dev`

`"postinstall": "npm run build"`

### key dependecies:

-   [ExpressJS](https://expressjs.com/)

### API:

The API is currently hosted on the cloud (i.e. ec2-18-185-83-59.eu-central-1.compute.amazonaws.com) and accepts both HTTP and HTTPS with CORS enabled on ports 8080 and 8443 respectively.

origin: `{host:8080}/api`

-   POST: `/sign-up { username, email, password }` - create a new user
-   POST: `/log-in { username, password }` - user login
-   POST: `/reset-password` - request for changing password
-   POST: `/new-password { token, password }` - create a new password
-   GET: `/user/me` - get user data
-   PATCH `/user/me { user data }` - update user data

### Test API:

You can test each one works with your browser.

-   https://ec2-18-185-83-59.eu-central-1.compute.amazonaws.com:8443/api

-   http://ec2-18-185-83-59.eu-central-1.compute.amazonaws.com:8080/api

### SSH Server:

The most updated ssl certificate can be requested by Isaak via Slack.

To connect, run command:

```
ssh -i <certificate> ubuntu@ec2-18-185-83-59.eu-central-1.compute.amazonaws.com
```
