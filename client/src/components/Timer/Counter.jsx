import styled from '@emotion/styled';

function calculateCircumference(radius) {
	return 2 * Math.PI * radius;
}

const SIZE = 82;
const radius = SIZE / 2;
const circleSize = calculateCircumference(radius);

const Counter = styled.div`
	position: relative;
	width: ${SIZE + 4}px;
	height: ${SIZE + 4}px;
	background: ${props => (props.restTime < 11 ? '#fff' : 'rgb(70, 48, 70)')};
	border-radius: 100%;
	line-height: ${SIZE + 4}px;
	text-align: center;
	color: ${props => (props.restTime < 11 ? '#f50057' : '#fff')};
	font-family: 'Annie Use Your Telescope', cursive;
	font-weight: bold;
	font-size: 28px;
	transition: 0.3s;

	svg {
		position: absolute;
		top: 0;
		right: 0;
		width: ${SIZE + 4}px;
		height: ${SIZE + 4}px;
		transform: rotateY(-180deg) rotateZ(-90deg);

		circle {
			stroke-dasharray: ${circleSize}px;
			stroke-dashoffset: 0px;
			stroke-linecap: round;
			stroke-width: 4px;
			stroke: ${props => (props.restTime < 11 ? '#f50057' : '#fff')};
			fill: none;
			animation: countdown ${props => props.seconds}s linear forwards;
		}
	}

	@keyframes countdown {
		from {
			stroke-dashoffset: 0px;
		}
		to {
			stroke-dashoffset: ${circleSize}px;
		}
	}
`;

export default Counter;
