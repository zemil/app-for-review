import React from 'react';
import styled from '@emotion/styled';

const UIButton = styled.button`
	line-height: 2;
	padding: 15px 30px;
	font-size: ${props => `${props.size}px` || '24px'};
	border-radius: 200px;
	border: 0;
	outline: 0;
	background-color: ${props => props.background || '#fa5596'};
	background: ${props =>
		props.background || 'linear-gradient(141deg, #c624a4 0%, #fb7e71 51%, #fa5795 75%)'};
	color: ${props => props.color || '#fff'};
	transition: 0.5s;
	cursor: pointer;

	&:hover {
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.55);
		opacity: 0.85;
	}
`;

function Button({ children, ...props }) {
	return <UIButton {...props}>{children}</UIButton>;
}

export default Button;
