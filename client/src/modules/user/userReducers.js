import {
	SUCCESS_LOGIN,
	ERROR_LOGIN,
	SET_USER,
	CHANGE_USER_FIELD,
	CHANGE_GAME_STAGE,
	START_GAME_ACTION,
	FINISH_GAME,
} from './userActions';

import { STAGES } from '../../utils/constants/game';

const initialState = {
	// search for production: STAGES.gameSearch
	gameStage: STAGES.gameSearch,
	isAuthenticated: true,
	name: '',
	birthday: '',
	gender: '',
	opponentGender: '',
	opponentAgeFrom: '',
	opponentAgeTo: '',
	game: {
		opponent: {
			username: 'Unknown',
		},
		roomId: '',
		winner: '',
		loser: '',
	},
};

function user(state = initialState, action) {
	switch (action.type) {
		case SUCCESS_LOGIN: {
			return {
				...state,
				isAuthenticated: true,
			};
		}

		case ERROR_LOGIN:
			return {
				...state,
				isAuthenticated: false,
			};

		case SET_USER: {
			return {
				...state,
				...action.data,
			};
		}

		case CHANGE_USER_FIELD: {
			return {
				...state,
				[action.field]: action.value,
			};
		}

		case CHANGE_GAME_STAGE: {
			return {
				...state,
				gameStage: action.stage,
			};
		}

		case START_GAME_ACTION: {
			return {
				...state,
				gameStage: STAGES.gameProcess,
				game: {
					...state.game,
					roomId: action.roomId,
					opponent: {
						...state.game.opponent,
						...action.opponent,
					},
				},
			};
		}

		case FINISH_GAME: {
			return {
				...state,
				gameStage: STAGES.gameEnd,
				game: {
					...state.game,
					winner: action.winner,
					loser: action.loser,
				},
			};
		}

		// just for example of simpleWebRTC actions
		// case '@andyet/LEAVE_CALL': {
		// 	console.log('LEAVE', 11111111);

		// 	return state;
		// }

		// case '@andyet/PEER_OFFLINE': {
		// 	console.log('PEER_OFFLINE', 444);

		// 	return state;
		// 	// return {
		// 	// 	...state,
		// 	// 	gameStage: STAGES.gameSearch,
		// 	// }
		// }

		default:
			return state;
	}
}

export default user;
