import React, { Component } from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import CloseIco from '@material-ui/icons/Close';

import { CONNECT_USER } from '../../utils/constants/socketEvents';

import Loader from '../../components/UI/Loader';
import Fab from '../../components/UI/Fab';
import BottomBar from '../../components/UI/BottomBar';
import BluredWrapper from '../../components/Layouts/BluredWrapper';

import logo from '../../media/images/logo-big.png';

const PlayersCount = styled.div`
	position: absolute;
	color: #fff;
	font-size: 20px;
	font-weight: 500;
	bottom: 200px;
	left: calc(50% - 110px);
	width: 220px;
	text-align: center;
	z-index: 10;
`;

class SearchGame extends Component {
	componentDidMount() {
		const {
			user: { username, age, gender, opponentAgeFrom, opponentAgeTo, opponentGender },
			socket,
		} = this.props;

		socket.emit(CONNECT_USER, {
			username,
			age,
			gender,
			opponentAgeFrom,
			opponentAgeTo,
			opponentGender,
		});
	}

	render() {
		const { count } = this.props;

		return (
			<>
				<BluredWrapper />

				<Loader inner={<img src={logo} alt="Spark2Date" />} />

				<PlayersCount>{count} Players Online looking for opponent...</PlayersCount>

				<BottomBar>
					<Fab component={Link} to="/mirror" size="large">
						<CloseIco />
					</Fab>
				</BottomBar>
			</>
		);
	}
}

export default SearchGame;
