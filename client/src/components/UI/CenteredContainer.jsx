import styled from '@emotion/styled';

const Container = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
	flex: 1;
	height: 100vh;
	width: 100vw;
`;

export default Container;
