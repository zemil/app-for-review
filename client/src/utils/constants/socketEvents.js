// duplicated on backend file: socket/events.js
export const CONNECT_USER = 'connect_user';

export const CHANGE_USERS_IN_SEARCH = 'change_users_in_search';

export const START_GAME = 'start_game';

export const GIVE_WIN = 'give_win';

export const FINISH_GAME = 'finish_game';
