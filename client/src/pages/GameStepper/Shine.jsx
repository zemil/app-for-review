import React, { Component } from 'react';
import styled from '@emotion/styled';
// import { Link } from 'react-router-dom';
import BackIco from '@material-ui/icons/Refresh';
import OkIco from '@material-ui/icons/PersonAdd';

import { STAGES } from '../../utils/constants/game';

import CenteredText from '../../components/UI/CenteredText';
import BluredWrapper from '../../components/Layouts/BluredWrapper';

import BottomBar from '../../components/UI/BottomBar';
import Fab from '../../components/UI/Fab';

const ShineCenteredText = styled(CenteredText)`
	font-size: 55px;
`;

const ShineBottomBar = styled(BottomBar)`
	position: fixed;
	left: 0;
	bottom: 50px;
	width: 100%;
	z-index: 99;
	display: flex;
	justify-content: center;
	align-items: center;

	button,
	a {
		margin: 0 10px;
	}
`;

class GameShine extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isShine: Math.random() > 0.5,
		};
	}

	componentWillUnmount() {
		const { changeGameStage } = this.props;

		changeGameStage(STAGES.gameSearch);
	}

	get Shine() {
		return (
			<>
				<BluredWrapper />

				<ShineCenteredText>
					It's
					<br />A Shine
				</ShineCenteredText>

				<ShineBottomBar>
					{/* <Fab component={Link} to="/mirror" size="large" title="To mirror">
						<BackIco />
					</Fab> */}
					{/* FIX: with reload cuz mirror works incorrect without reload */}
					<Fab href="/mirror" size="large" title="To mirror">
						<BackIco />
					</Fab>

					<Fab
						color="primary"
						variant="extended"
						size="large"
						// component={Link}
						href="/mirror"
					>
						Add to friends
						<OkIco />
					</Fab>
				</ShineBottomBar>
			</>
		);
	}

	get NoShine() {
		return (
			<>
				<BluredWrapper />

				<ShineCenteredText>
					Sorry,
					<br />
					No
					<br />
					Shine
				</ShineCenteredText>

				<BottomBar>
					<Fab
						// component={Link}
						// to="/mirror"
						href="/mirror"
						size="large"
						title="To mirror"
					>
						<BackIco />
					</Fab>
				</BottomBar>
			</>
		);
	}

	render() {
		const { isShine } = this.state;

		return isShine ? this.Shine : this.NoShine;
	}
}

export default GameShine;
