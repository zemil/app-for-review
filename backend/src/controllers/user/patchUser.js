import User from '../../schemes/user';
import decodeJWT from '../../utils/helpers/decodeJWT';

function patchUser(req, res) {
    try {
        const {
            name,
            gender,
            birthday,
            opponentGender,
            opponentAgeFrom,
            opponentAgeTo
        } = req.body;
        const { userId } = decodeJWT(req);

        User.findOneAndUpdate(
            { _id: userId },
            {
                $set: {
                    name,
                    gender,
                    opponentGender,
                    birthday,
                    opponentAgeFrom,
                    opponentAgeTo
                }
            },
            { new: true },
            (err, user) => {
                if (err) {
                    res.status(500).send({
                        success: false,
                        message: err
                    });
                } else {
                    // TODO:? send user data {...user}
                    // for now success: true is enought
                    res.json({
                        success: true
                    });
                }
            }
        );
    } catch (err) {
        res.json({ success: false });

        throw `patch user error: ${err}`;
    }
}

export default patchUser;
