import bad from './bad.png';
import neutral from './ok.png';
import good from './good.png';
import spark from './spark.png';

export default {
	bad,
	neutral,
	good,
	spark,
};
