import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import styled from '@emotion/styled';

import { login } from '../../modules/user/userActions';

import AuthLayout from './Layout';
import Fab from '../../components/UI/Fab';
import TextField from '../../components/UI/TextField';

const GreyLink = styled(Link)`
	display: block;
	text-align: center;
	color: #909090;
	text-decoration: underline;
`;

const LinksBlock = styled.div`
	padding: 10px 0;
	display: flex;
	justify-content: space-between;
`;

class LoginSignup extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
			password: '',
			invalidFields: [],
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
	}

	handleChange(e) {
		const { name, value } = e.target;

		this.setState({
			[name]: value,
		});
	}

	get isValidForm() {
		const { username, password } = this.state;
		const invalidFields = [];

		if (username.trim().length < 4) {
			invalidFields.push('username');
		}
		if (password.length < 8) {
			invalidFields.push('password');
		}

		this.setState({
			invalidFields,
		});

		return invalidFields.length === 0;
	}

	handleSubmitLogin(e) {
		e.preventDefault();

		const { username, password } = this.state;

		if (this.isValidForm) {
			const { login } = this.props;
			const data = {
				username,
				password,
			};

			login(data);
		}
	}

	render() {
		const { username, password, invalidFields } = this.state;

		return (
			<>
				<AuthLayout onSubmit={this.handleSubmitLogin}>
					<TextField
						error={invalidFields.includes('username')}
						label="Username or email"
						type="text"
						name="username"
						margin="normal"
						variant="outlined"
						value={username}
						onChange={this.handleChange}
					/>

					<TextField
						error={invalidFields.includes('password')}
						label="Password"
						type="password"
						autoComplete="current-password"
						name="password"
						margin="normal"
						variant="outlined"
						value={password}
						onChange={this.handleChange}
					/>

					<Fab variant="extended" color="primary" type="submit">
						Log in
					</Fab>

					<LinksBlock>
						<GreyLink to="/sign-up">Sign up</GreyLink>

						<GreyLink to="/reset-password">Remind a password</GreyLink>
					</LinksBlock>
				</AuthLayout>
			</>
		);
	}
}

export default connect(
	null,
	{
		login,
	}
)(LoginSignup);
