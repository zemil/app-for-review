import React, { Component } from 'react';
import * as SWRTC from '@andyet/simplewebrtc';
import * as faceapi from 'face-api.js';
import CloseIco from '@material-ui/icons/Close';
import ViewIco from '@material-ui/icons/Face';
import WinIco from '@material-ui/icons/FavoriteBorder';
import { Link } from 'react-router-dom';

import helpers from '../../../utils/helpers';
import { CARDS } from '../../../utils/constants/game';
import { GIVE_WIN, FINISH_GAME } from '../../../utils/constants/socketEvents';

import Timer from '../../../components/Timer';
import Mirror from '../../../components/Mirror';
import Fab from '../../../components/UI/Fab';
import Loader from '../../../components/UI/Loader';
import CenteredText from '../../../components/UI/CenteredText';
import {
	BluredWrapper,
	OpponentVideoContainer,
	FaceNotDetectedContainer,
	ProcessBottomBar,
	WaitingWrapper,
	TopBar,
	WebrtcContainer,
} from './Styled';

const { getURLParam } = helpers;

const MODEL_URL = 'models';

const card = CARDS[Math.floor(Math.random() * CARDS.length)];

class GameProcess extends Component {
	constructor(props) {
		super(props);

		// it needs for test/dev purposes
		const roomByUrl = getURLParam('room');

		this.state = {
			roomByUrl,
			// change on false when face regognition will be finished
			faceDetected: true,
		};

		this.detectFrame = this.detectFrame.bind(this);
		this.handleFinish = this.handleFinish.bind(this);
		this.giveWin = this.giveWin.bind(this);
	}

	componentDidMount() {
		const { socket, finishGame } = this.props;

		// TODO: nned to finish face recognition
		// repo: https://github.com/iZemil/face-detection-validation
		// this.loadModels();

		// SOCKETS:
		socket.on(FINISH_GAME, finishGameData => {
			const { winner, loser } = finishGameData;

			finishGame({ winner, loser });
		});
	}

	async loadModels() {
		try {
			await faceapi.loadFaceDetectionModel(MODEL_URL);
			// await faceapi.loadFaceLandmarkModel(MODEL_URL);
			// await faceapi.loadFaceRecognitionModel(MODEL_URL);

			// then:
			this.startDetect();
		} catch (error) {
			console.error('Load models error: ', error);
		}
	}

	localView(localVideo) {
		if (localVideo) {
			return (
				<div>
					<SWRTC.Video media={localVideo} />
					<SWRTC.Audio media={localVideo} />
				</div>
			);
		}
	}

	remoteView(media) {
		if (media) {
			const { faceDetected } = this.state;

			return (
				<OpponentVideoContainer faceDetected={faceDetected}>
					<SWRTC.Video media={media} />
					<SWRTC.Audio media={media} />
				</OpponentVideoContainer>
			);
		}
	}

	async detectFrame() {
		if (
			!this.state.faceDetected &&
			this.videoRef != null &&
			this.videoRef.children != null &&
			this.videoRef.children[0] != null
		) {
			if (
				await faceapi.detectSingleFace(
					this.videoRef.children[0],
					new faceapi.TinyFaceDetectorOptions()
				)
			) {
				this.setState({
					faceDetected: true,
				});
			}
		}
		if (!this.state.faceDetected) {
			requestAnimationFrame(() => {
				this.detectFrame();
			});
		}
	}

	handleFinish() {
		const { finishGame } = this.props;

		finishGame({ winner: '', loser: '' });
	}

	giveWin() {
		const {
			socket,
			user: {
				name,
				game: {
					opponent: { username: opponentName },
				},
			},
			finishGame,
		} = this.props;

		const winData = { winner: opponentName, loser: name };

		socket.emit(GIVE_WIN, winData);

		finishGame(winData);
	}

	render() {
		const { roomByUrl, faceDetected } = this.state;
		const {
			webrtc,
			user: {
				game: {
					opponent: { username: opponentName, age: opponentAge },
					roomId,
				},
			},
		} = this.props;
		const { peers } = webrtc;
		const isOpponentConnected = Object.keys(peers).length > 0;

		return (
			<div>
				<TopBar>
					<div className="opponent">
						<div title="Your opponent">
							{opponentName}, {opponentAge}
						</div>
						<b className="cursive">«{card}»</b>
						<Fab component={Link} to="/mirror" size="small" className="close-btn">
							<CloseIco />
						</Fab>
					</div>

					<div className="topbar__row">
						{isOpponentConnected && (
							<Timer value={120} handleFinish={this.handleFinish} />
						)}

						<div className="my-face" title="Your face">
							<Mirror />
						</div>
					</div>
				</TopBar>

				<WebrtcContainer>
					<SWRTC.Disconnected>
						<h2>Connecting...</h2>
					</SWRTC.Disconnected>

					<SWRTC.Connected>
						<SWRTC.RequestUserMedia audio video auto />

						<SWRTC.Room name={roomId || roomByUrl} password="spark2date">
							{({ peers, remoteMedia, localMedia }) => {
								// const remoteVideos = remoteMedia.filter(m => m.kind === 'video');
								const localVideo = localMedia[0];
								const remoteVideo = remoteMedia[0];
								const isNoOpponent = peers.length === 0;

								if (isNoOpponent) {
									return (
										<WaitingWrapper>
											<Loader
												inner={
													<div className="cursive">
														Waiting your opponent...
													</div>
												}
											/>
										</WaitingWrapper>
									);
								}

								return (
									<>
										{this.localView(localVideo)}

										{this.remoteView(remoteVideo)}

										{faceDetected ? (
											<ProcessBottomBar>
												<Fab
													color="primary"
													variant="extended"
													size="large"
													onClick={this.giveWin}
												>
													Give a win
													<WinIco />
												</Fab>
											</ProcessBottomBar>
										) : (
											<>
												<FaceNotDetectedContainer>
													<BluredWrapper />
													<CenteredText>
														Opponent
														<br />
														face is not
														<br />
														detected yet
													</CenteredText>
												</FaceNotDetectedContainer>

												<ProcessBottomBar>
													<Fab
														color="secondary"
														size="large"
														component={Link}
														to="/mirror"
													>
														<CloseIco />
													</Fab>

													<Fab
														variant="extended"
														size="large"
														onClick={this.detectFrame}
													>
														Don't Care
														<ViewIco />
													</Fab>
												</ProcessBottomBar>
											</>
										)}
									</>
								);
							}}
						</SWRTC.Room>
					</SWRTC.Connected>
				</WebrtcContainer>
			</div>
		);
	}
}

export default GameProcess;
