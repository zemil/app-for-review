import React from 'react';
import styled from '@emotion/styled';

import Container from '../../components/UI/CenteredContainer';

import logo from '../../media/images/logo-big.png';

const CenteredImage = styled.img`
	margin: 0 auto;
	animation: beat 0.9s infinite alternate;
	transform-origin: center;

	@keyframes beat {
		to {
			transform: scale(0.95);
		}
	}
`;

const LoginSignupBlock = styled.form`
	display: flex;
	width: 100%;
	max-width: 320px;
	flex: 1;
	flex-direction: column;
	justify-content: center;

	button {
		margin: 20px 0;
	}
`;

function AuthLayout({ onSubmit, children }) {
	return (
		<Container>
			<LoginSignupBlock onSubmit={onSubmit}>
				<CenteredImage src={logo} alt="Spark2Date" width="125" height="125" />

				{children}
			</LoginSignupBlock>
		</Container>
	);
}

export default AuthLayout;
