import User from '../../schemes/user';

function newPassword(req, res) {
    const { token, password } = req.body;

    if (token.trim().length === 0) {
        res.json({
            success: false,
            message: 'You should send a token'
        });
    }

    User.findOne(
        {
            resetPasswordToken: token
        },
        (error, user) => {
            if (error) {
                res.json({
                    success: false,
                    message: error
                });
            }

            if (password.length < 8) {
                res.json({
                    success: false,
                    message: 'Invalid password'
                });
            }

            if (!user) {
                res.json({
                    success: false
                });
            } else if (user) {
                // user exists:
                const tokenDate = moment(user.resetPasswordDate);
                const currentDate = moment(Date.now());

                const restHours = currentDate.diff(tokenDate, 'hours');

                if (restHours > 4) {
                    res.json({
                        success: false,
                        message: 'Token is invalid'
                    });
                } else {
                    user.password = password;
                    user.resetPasswordToken = '';

                    res.json({
                        success: true,
                        message: 'Password was changed'
                    });

                    user.save();
                }
            }
        }
    );
}

export default newPassword;
