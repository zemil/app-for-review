import styled from '@emotion/styled';

const FullScreen = styled.div`
	height: 100vh;
	width: 100%;
	overflow: hidden;
`;

export default FullScreen;
