const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    target: 'node',
    externals: [nodeExternals()],

    entry: ['babel-polyfill', './src/index.js'],

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },

    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
                include: path.join(__dirname, 'src')
            }
        ]
    },

    plugins: [
        new CleanWebpackPlugin(['build'], {
            root: path.resolve(__dirname, ''),
            verbose: true
        }),
        new webpack.HotModuleReplacementPlugin()
    ]
};
