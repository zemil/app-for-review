import jwt from 'jsonwebtoken';

import { JWT_SECRET } from '../utils/constants';

// TODO: optimize it
export const checkAuth = (req, res, next) => {
    // check header or url parameters or post parameters for token
    try {
        const token =
            req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, JWT_SECRET, function(err, decoded) {
                console.log(err);
                if (err) {
                    // Redirect
                    return res.status(401).redirect('/login');
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    } catch (err) {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
};
