'use strict';

module.exports.login = async (event, context) => {
	const { username, email, password } = event.body;
	let error, foundUser;

	if (!username || !password) {
		error = `please provide at least password and username`;
	} else if (!foundUser) {
		error = `username ${username} not found`;
	}

	if (error) {
		return {
			statusCode: 400,
			body: JSON.stringify({
				isError: true,
				error,
			}),
		};
	} else {
		return {
			statusCode: 200,
			body: JSON.stringify({
				isError: false,
				username,
			}),
		};
	}

	// Use this code if you don't use the http event with the LAMBDA-PROXY integration
	// return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.register = async (event, context) => {
	const { username, email, password } = event.body;
	return {
		statusCode: 200,
		body: JSON.stringify({
			message: 'Registering',
			input: event,
		}),
	};
};
