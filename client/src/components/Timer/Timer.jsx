import React, { Component } from 'react';

import Counter from './Counter';

export default class Timer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			seconds: props.value,
			time: 0,
			isOn: false,
			start: 0,
		};

		this.startTimer = this.startTimer.bind(this);
		this.stopTimer = this.stopTimer.bind(this);
		this.resetTimer = this.resetTimer.bind(this);
	}

	componentDidMount() {
		this.startTimer();
	}

	componentWillUnmount() {
		clearInterval(this.timerId);
	}

	startTimer() {
		const { time } = this.state;

		this.setState({
			isOn: true,
			time,
			start: Date.now() - time,
		});

		this.timerId = setInterval(() => {
			const { start, seconds } = this.state;
			const time = Date.now() - start;
			const restTime = Math.round(seconds - time / 1000);

			this.setState({
				time,
				restTime,
			});

			if (restTime === 0) {
				this.stopTimer();
			}
		}, 1);
	}

	stopTimer() {
		const { handleFinish } = this.props;

		this.setState({ isOn: false });

		clearInterval(this.timerId);

		if (handleFinish) {
			handleFinish();
		}
	}

	resetTimer() {
		this.setState({ time: 0, isOn: false });
	}

	render() {
		const { restTime, seconds } = this.state;

		return (
			<Counter restTime={restTime} seconds={seconds}>
				{restTime}

				<svg>
					<circle r="41" cx="43" cy="43" />
				</svg>
			</Counter>
		);
	}
}
