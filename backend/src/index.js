import express from 'express';
import https from 'https';
import http from 'http';
import cors from 'cors';
import bodyParser from 'body-parser';
import credentials from '../server/credentials';
import initSockets from './socket';

import userRoutes from './routes/user';

const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT || 8080;
const PORT_SSL = 8443;
const db = require('./db');
const app = express();

app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.get('/', (req, res) => res.send('<b>Spark2Date REST API is working</b>'));
app.use('/api', userRoutes);

const server = http
    .createServer(app)
    .listen(PORT, () => console.log(`HTTP server was started on: ${PORT}`));
const serverSecure = https
    .createServer(credentials, app)
    .listen(PORT_SSL, () =>
        console.log(`HTTPS server was started on: ${PORT_SSL}`)
    );

/**
 * SOCKET.IO:
 *
 */
initSockets(server);
