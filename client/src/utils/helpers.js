import { email } from './constants/regex';

const helpers = {
	isEmail(value) {
		return email.test(value);
	},

	getURLParam(param) {
		const urlObj = new URL(window.location.href);
		return urlObj.searchParams.get(param);
	},

	/**
	 * create custom delay
	 * @param {Number} ms - wait milliseconds
	 */
	wait(ms = 2000) {
		return new Promise(res => setTimeout(res, ms));
	},

	/**
	 * remove html symbols
	 * @param {String} stringWithHTML
	 */
	removeHTML(stringWithHTML) {
		return stringWithHTML.replace(/(<[^>]*>)|(\s)/g, '');
	},
};

export default helpers;
