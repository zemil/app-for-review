import styled from '@emotion/styled';

const CenteredText = styled.div`
	position: absolute;
	top: 50%;
	left: 0;
	transform: translateY(-50%);
	color: #fff;
	font-size: 45px;
	font-weight: 500;
	width: 100%;
	text-align: center;
	font-family: 'Annie Use Your Telescope', cursive;
`;

export default CenteredText;
