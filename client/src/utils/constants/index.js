export const API_KEY = '25b28549c017958081134e54';

export const AUTH_KEY = '__spark2date_auth_key__';

export const API_URL = (function() {
	let secure = window.location.protocol.search('https') !== -1;
	return process.env.NODE_ENV === 'production'
		? `http${secure ? 's' : ''}://ec2-18-185-83-59.eu-central-1.compute.amazonaws.com:${
				secure ? 8443 : 8080
		  }`
		: `http${secure ? 's' : ''}://localhost:${secure ? 8443 : 8080}`;
})();

export const CONFIG_URL = `https://api.simplewebrtc.com/config/guest/${API_KEY}`;
