import { Router } from 'express';
import {
    createNewUser,
    loginUser,
    resetPassword,
    newPassword,
    getUser,
    patchUser
} from '../controllers/user';
import { checkAuth } from '../utils/middlewares';

const router = Router();

// public routes
router.post('/sign-up', createNewUser);

router.post('/log-in', loginUser);

router.post('/reset-password', resetPassword);

router.post('/new-password', newPassword);

// protection middleware
router.use(checkAuth);

// private routes
router.get('/user/me', getUser);

router.patch('/user/me', patchUser);

export default router;
