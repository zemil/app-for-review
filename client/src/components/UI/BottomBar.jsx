import styled from '@emotion/styled';

const BottomBar = styled.div`
	position: fixed;
	left: 0;
	bottom: 50px;
	width: 100%;
	z-index: 99;
	text-align: center;
`;

export default BottomBar;
