import React from 'react';
import styled from '@emotion/styled';
import * as faceapi from 'face-api.js';

const Mirror = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	text-align: center;
	background: rgba(179, 17, 179, 0.25);

	.to-game {
		position: absolute;
		left: 50%;
		bottom: 100px;
		transform: translate(-50%, 0);
	}

	.primary {
		border-radius: 200px !important;
		font-size: 4vw;
		opacity: 0.66;
		transition: 0.3s;

		&:hover {
			opacity: 1;
		}
	}
`;

// Video sizes
const WIDTH = 'auto';
const HEIGHT = '100%';
const MODEL_URL = 'models';

function resizeCanvasAndResults(dimensions, canvas, results) {
	const { width, height } =
		dimensions instanceof HTMLVideoElement
			? faceapi.getMediaDimensions(dimensions)
			: dimensions;
	canvas.width = width;
	canvas.height = height;

	// resize detections (and landmarks) in case displayed image is smaller than
	// original size
	return faceapi.resizeResults(results, { width, height });
}

class MirrorPage extends React.Component {
	async componentDidMount() {
		await this.loadModels();

		this.loadWebCam();
	}

	componentWillUnmount() {
		try {
			const stream = this.videoRef.srcObject;
			const tracks = stream.getTracks();

			tracks.forEach(track => {
				track.stop();
			});

			this.videoRef.srcObject = null;
		} catch (error) {
			console.warn('stop mirror error: ', error);
		}
	}

	async loadModels() {
		try {
			await faceapi.loadFaceDetectionModel(MODEL_URL);
			// await faceapi.loadFaceLandmarkModel(MODEL_URL);
			// await faceapi.loadFaceRecognitionModel(MODEL_URL);

			// then:
			// this.startDetect();
		} catch (error) {
			console.error('Load models error: ', error);
		}
	}

	loadWebCam() {
		const video = this.videoRef;

		navigator.mediaDevices
			.getUserMedia({
				audio: false,
				video: {
					facingMode: 'user',
				},
			})
			.then(stream => {
				video.srcObject = stream;

				return new Promise((resolve, reject) => {
					video.onloadedmetadata = () => {
						video.play();

						resolve();
					};
				});
			})
			.then(() => this.detectFrame())
			.catch(err => {
				console.warn('getUserMedia Error: ', err);
			});
	}

	async detectFrame() {
		const minConfidence = 0.5;
		const options = new faceapi.SsdMobilenetv1Options({ minConfidence });
		const result = await faceapi.detectSingleFace(this.videoRef, options);

		if (result) {
			if (window.location.pathname === '/test') {
				const resizedDetections = resizeCanvasAndResults(this.videoRef, this.canvasRef, [
					result,
				]);

				faceapi.drawDetection(this.canvasRef, resizedDetections);
			}
			// if (!this.statuses.isDetected) {
			// 	set(this.statuses, {
			// 		isDetected: true,
			// 		isFetching: true,
			// 	});
			// }
		}

		requestAnimationFrame(() => {
			this.detectFrame();
		});
	}

	render() {
		return (
			<Mirror>
				<video
					id="video"
					ref={el => {
						this.videoRef = el;
					}}
					width={WIDTH}
					height={HEIGHT}
				/>

				<canvas
					id="canvas"
					ref={el => {
						this.canvasRef = el;
					}}
					width={WIDTH}
					height={HEIGHT}
					style={{
						position: 'fixed',
						top: 0,
						bottom: 0,
						left: 0,
						right: 0,
						width: '100%',
						height: '100%',
						zIndex: 999,
					}}
				/>
			</Mirror>
		);
	}
}

export default MirrorPage;
