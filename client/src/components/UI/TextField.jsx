import React from 'react';
import styled from '@emotion/styled';
import TextField from '@material-ui/core/TextField';

const StyledTextField = styled(TextField)``;

function TextFieldUI({ ...props }) {
	return <StyledTextField {...props} />;
}

export default TextFieldUI;
