/**
 *
 *  it finds pair user index for the first user
 *  returns the Index
 *  or Null
 *  */
export default function getValidPairIndex(users) {
    const usersCount = users.length;

    if (usersCount > 1) {
        const user1 = users[0];
        const { opponentGender, opponentAgeFrom, opponentAgeTo } = user1;

        // search pair for user1:
        for (let userIndex = 1; userIndex < usersCount; userIndex += 1) {
            const user = users[userIndex];
            const { gender, age } = user;

            const validGender =
                opponentGender === 'both' || opponentGender === gender;
            const ageInRange = age >= opponentAgeFrom && age <= opponentAgeTo;
            const validPair = validGender && ageInRange;

            if (validPair) {
                return userIndex;
            }
        }
    }

    return null;
}
