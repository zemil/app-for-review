import User from '../../schemes/user';
import decodeJWT from '../../utils/helpers/decodeJWT';

function getUser(req, res) {
    try {
        const { userId } = decodeJWT(req);

        User.findById(userId, (err, user) => {
            if (!err) {
                const {
                    name,
                    username,
                    email,
                    age,
                    gender,
                    birthday,
                    opponentGender,
                    opponentAgeFrom,
                    opponentAgeTo
                } = user;

                res.json({
                    data: {
                        name,
                        username,
                        email,
                        age,
                        birthday,
                        gender,
                        opponentGender,
                        opponentAgeFrom,
                        opponentAgeTo
                    },
                    success: true
                });
            }
        });
    } catch (err) {
        res.json({ success: false });

        throw `get user error: ${err}`;
    }
}

export default getUser;
