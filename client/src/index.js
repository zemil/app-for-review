import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root';
import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer as simplewebrtc } from '@andyet/simplewebrtc';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';

import reducers from './modules';
import { checkAuth } from './modules/user/userActions';

import './media/styles/index.scss';

const store = createStore(
	combineReducers({ simplewebrtc, ...reducers }),
	composeWithDevTools(applyMiddleware(thunk))
);
store.dispatch(checkAuth());

ReactDOM.render(
	<Provider store={store}>
		<>
			<CssBaseline />
			<Root />
		</>
	</Provider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
