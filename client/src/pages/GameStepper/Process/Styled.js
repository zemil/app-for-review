import styled from '@emotion/styled';

import BottomBar from '../../../components/UI/BottomBar';

// TODO: check it in components/Layouts/BluredWrapper
export const BluredWrapper = styled.div`
	width: 100%;
	height: 100%;
	background: rgba(179, 17, 179, 0.25);
`;

export const OpponentVideoContainer = styled.div`
	position: fixed;
	width: 100%;
	height: 100%;
	filter: ${props => (props.faceDetected ? 'initial' : 'blur(20px)')};
`;

export const FaceNotDetectedContainer = styled.div`
	position: fixed;
	width: 100%;
	height: 100%;
`;

export const ProcessBottomBar = styled(BottomBar)`
	display: flex;
	justify-content: center;
	align-items: center;

	button,
	a {
		margin: 0 10px;
	}
`;

export const WaitingWrapper = styled.div`
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	background: #c9c9c9;

	.Loader {
		font-size: 22px;
		color: #fff;
		font-weight: bold;
	}
`;

export const TopBar = styled.div`
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	z-index: 99;

	.topbar__row {
		display: flex;
		justify-content: flex-end;
		padding: 20px;
	}

	.opponent {
		padding: 20px 0;
		font-size: 18px;
		color: #d4d4d4;
		text-align: center;
		background: rgba(0, 0, 0, 0.5);
		box-shadow: 0px 8px 8px rgba(0, 0, 0, 0.1);
		border-bottom-left-radius: 100%;
		border-bottom-right-radius: 100%;

		.cursive {
			font-size: 24px;
			color: #fff;
		}
	}

	.my-face {
		position: relative;
		width: 100px;
		height: 100px;
		background: rgba(36, 36, 36, 0.85);
		border-radius: 12px;
		border: 2px solid #ccc;
		overflow: hidden;
		margin-left: auto;
	}

	.close-btn {
		position: absolute;
		right: 40px;
		top: 10px;
	}
`;

export const WebrtcContainer = styled.div`
	position: fixed;
	top: 0;

	video {
		position: fixed;
		left: 0;
		top: 50%;
		width: 100%;
		height: auto;
		transform: scaleX(-1) translatey(-50%) !important;
	}
`;
