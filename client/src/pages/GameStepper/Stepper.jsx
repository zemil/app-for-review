import React, { Component } from 'react';
import { connect } from 'react-redux';
import socketIOClient from 'socket.io-client';

import { STAGES } from '../../utils/constants/game';
import { API_URL } from '../../utils/constants';
import { START_GAME, CHANGE_USERS_IN_SEARCH } from '../../utils/constants/socketEvents';
import { changeGameStage, startGame, finishGame } from '../../modules/user/userActions';
import helpers from '../../utils/helpers';

import PreloaderScreen from '../../components/Layouts/PreloaderScreen';
import FullScreen from '../../components/Layouts/FullScreen';
import SearchGame from './Search';
import GameProcess from './Process';
import GameEnd from './End';
import GameShine from './Shine';

const { wait } = helpers;

class Game extends Component {
	constructor(props) {
		super(props);

		this.state = {
			count: 0,
			isFetching: true,
		};
	}

	async componentDidMount() {
		const { startGame } = this.props;

		this.socket = socketIOClient(API_URL);

		this.socket.on(CHANGE_USERS_IN_SEARCH, connectedList => {
			console.log('Users in search: ', connectedList);
			this.setState({ count: connectedList.length });
		});

		// go to room
		this.socket.on(START_GAME, data => {
			const { roomId, opponent } = data;
			console.log(roomId, opponent);

			startGame({ roomId, opponent });
		});

		// Preloader delay
		await wait(1500);

		this.setState({ isFetching: false });
	}

	renderStage() {
		const { count } = this.state;
		const { webrtc, user, changeGameStage, finishGame } = this.props;
		const { gameStage } = user;

		// TODO: need fetching flag and some preloader while user is loading
		// may be it needs to locate in another component
		console.log(999, this.socket);
		const isLoaded = user.username && this.socket;
		if (!isLoaded) {
			return null;
		}

		switch (gameStage) {
			case STAGES.gameSearch: {
				return <SearchGame count={count} user={user} socket={this.socket} />;
			}
			case STAGES.gameProcess: {
				return (
					<GameProcess
						webrtc={webrtc}
						user={user}
						changeGameStage={changeGameStage}
						socket={this.socket}
						finishGame={finishGame}
					/>
				);
			}
			case STAGES.gameEnd: {
				return <GameEnd changeGameStage={changeGameStage} user={user} />;
			}
			case STAGES.gameShine: {
				return <GameShine changeGameStage={changeGameStage} />;
			}
			default:
				return null;
		}
	}

	render() {
		const { isFetching } = this.state;

		return isFetching ? <PreloaderScreen /> : <FullScreen>{this.renderStage()}</FullScreen>;
	}
}

const mapStateToProps = state => ({
	webrtc: state.simplewebrtc,
	user: state.user,
});

export default connect(
	mapStateToProps,
	{ changeGameStage, startGame, finishGame }
)(Game);
