import React, { Component } from 'react';
import styled from '@emotion/styled';

import { STAGES, RATES } from '../../utils/constants/game';

import CenteredContainer from '../../components/UI/CenteredContainer';
import CenteredText from '../../components/UI/CenteredText';
import BottomBar from '../../components/UI/BottomBar';

import ratesImages from '../../media/images/rates';

import woman from '../../media/images/woman.jpg';

const EndBottomBar = styled(BottomBar)`
	.rates-text {
		margin-bottom: 10px;
		color: #fff;
	}

	.rates {
		display: flex;
		align-items: center;
		justify-content: center;

		img {
			max-width: 65px;
			height: auto;
			margin: 0 10px;
			cursor: pointer;
			transition: 0.3s;

			&:hover {
				transform: scale(1.2);
			}
		}
	}
`;

const OpponentFace = styled.div`
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	/* background: url(${woman}) no-repeat; */
	background-size: cover;

	&::before {
		content: '';
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.33);
	}
`;

class GameEnd extends Component {
	constructor(props) {
		super(props);

		this.changeGameStage = this.changeGameStage.bind(this);
	}

	changeGameStage(rate) {
		const { changeGameStage } = this.props;

		changeGameStage(STAGES.gameShine);
	}

	get gameResultView() {
		const {
			user: {
				username,
				game: { winner, loser },
			},
		} = this.props;

		if (winner && loser) {
			return winner === username ? (
				<CenteredText>You Won!</CenteredText>
			) : (
				<CenteredText>
					Your Opponent
					<br />
					Won!
				</CenteredText>
			);
		}

		return (
			<CenteredText>
				Time's up
				<br />
				Draw!
			</CenteredText>
		);
	}

	render() {
		return (
			<CenteredContainer style={{ background: 'rgba(0, 0, 0, 0.5)' }}>
				<OpponentFace />

				{this.gameResultView}

				<EndBottomBar>
					<div className="rates-text">rate the round, or spark to opponent!</div>

					<div className="rates">
						{RATES.map(rate => (
							<img
								key={rate}
								src={ratesImages[rate]}
								alt={rate}
								title={rate}
								onClick={() => this.changeGameStage(rate)}
							/>
						))}
					</div>
				</EndBottomBar>
			</CenteredContainer>
		);
	}
}

export default GameEnd;
