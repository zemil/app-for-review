const fs = require('fs');

export default {
    key: fs.readFileSync('./server/server.key', 'utf8'),
    cert: fs.readFileSync('./server/server.crt', 'utf8')
};
