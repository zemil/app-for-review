import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Snackbar, Slide } from '@material-ui/core';

import api from '../../utils/api';
import helpers from '../../utils/helpers';

import Fab from '../../components/UI/Fab';
import TextField from '../../components/UI/TextField';
import AuthLayout from './Layout';

class ResetPassword extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			invalidFields: [],
			isVisibleSnack: false,
		};

		this.toggleSnackbar = this.toggleSnackbar.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	toggleSnackbar(message) {
		const { isVisibleSnack } = this.state;

		this.setState({
			isVisibleSnack: !isVisibleSnack,
			message: message ? message : '',
		});
	}

	handleChange(e) {
		const { name, value } = e.target;

		this.setState({
			[name]: value,
		});
	}

	handleSubmit(e) {
		e.preventDefault();

		const { email } = this.state;

		const data = {
			email,
		};

		if (!helpers.isEmail(email)) {
			this.setState({ invalidFields: ['email'] });
			return;
		} else {
			this.setState({ invalidFields: [] });
		}

		api.post('reset-password', data)
			.then(data => data.json())
			.then(json => {
				if (json.success) {
					// success
					this.toggleSnackbar('We sent information to your email');
				} else {
					alert('Login problem, check your data');
				}
			})
			.catch(err => {
				console.error('pass error: ', err);
				this.toggleSnackbar('Oops! Password error');
			});
	}

	render() {
		const { email, invalidFields, isVisibleSnack, message } = this.state;

		return (
			<AuthLayout onSubmit={this.handleSubmit}>
				<TextField
					error={invalidFields.includes('email')}
					label="Your email"
					name="email"
					type="email"
					value={email}
					margin="normal"
					variant="outlined"
					onChange={this.handleChange}
				/>

				<Fab variant="extended" color="primary" type="submit">
					Remind me a password
				</Fab>

				<Fab variant="extended" component={Link} to="/login">
					Back
				</Fab>

				<Snackbar
					autoHideDuration={4000}
					open={isVisibleSnack}
					onClick={() => this.toggleSnackbar()}
					TransitionComponent={props => <Slide {...props} direction="up" />}
					message={<span>{message}</span>}
				/>
			</AuthLayout>
		);
	}
}

export default ResetPassword;
