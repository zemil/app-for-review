import api from '../../utils/api';
import ll from '../../utils/localStorage';

export const SUCCESS_LOGIN = 'success_login';
function successLogin() {
	return {
		type: SUCCESS_LOGIN,
	};
}

export const ERROR_LOGIN = 'error_login';
function errorLogin() {
	ll.remove('usertoken');
	return {
		type: ERROR_LOGIN,
	};
}

export function login(data) {
	return async dispatch => {
		try {
			const loginRes = await api.post('log-in', data);
			const loginData = await loginRes.json();

			const { success, token } = loginData;

			if (success) {
				ll.set('usertoken', token);

				dispatch(successLogin());

				// TODO: pushState need without reload:
				// https://stackoverflow.com/questions/32612418/transition-to-another-route-on-successful-async-redux-action
				window.location = '/about';
			} else {
				alert('Login problem, check your data');

				dispatch(errorLogin());
			}
		} catch (err) {
			console.error('log in error: ', err);
		}
	};
}

export function checkAuth() {
	const isAuth = ll.get('usertoken');

	return async dispatch => {
		if (isAuth) {
			dispatch(successLogin());

			try {
				const userRes = await getCurrentUser();

				const { success, data } = userRes;

				if (success) {
					dispatch(successLogin());
					dispatch(setCurrentUser(data));

					return;
				}
			} catch (error) {
				console.error('getCurrentUser error: ', error);

				// only for client development !!!
				// uncomment it if you work without server:
				// return dispatch(successLogin());
			}
		}

		dispatch(errorLogin());
	};
}

export async function getCurrentUser() {
	const res = await api.get('user/me');
	const resData = await res.json();

	return resData;
}

export const SET_USER = 'set_user';
export function setCurrentUser(data) {
	console.log(data);

	return dispatch =>
		dispatch({
			type: SET_USER,
			data,
		});
}

export const CHANGE_USER_FIELD = 'change_user_data';
export function changeUserField(field, value) {
	return {
		type: CHANGE_USER_FIELD,
		field,
		value,
	};
}

export function saveUserData() {
	return (_dispatch, getState) => {
		const {
			user: { name, birthday, gender, opponentGender, opponentAgeFrom, opponentAgeTo },
		} = getState();

		const data = {
			name,
			birthday,
			gender,
			opponentGender,
			opponentAgeFrom,
			opponentAgeTo,
		};

		api.patch('user/me', data)
			.then(data => data.json())
			.then(json => json)
			.catch(err => alert('Save user data error occured'));
	};
}

export const CHANGE_GAME_STAGE = 'change_game_stage';
export function changeGameStage(stage) {
	return {
		type: CHANGE_GAME_STAGE,
		stage,
	};
}

export const START_GAME_ACTION = 'start_game_action';
export function startGame({ roomId, opponent }) {
	return {
		type: START_GAME_ACTION,
		roomId,
		opponent,
	};
}

export const FINISH_GAME = 'finish_game';
export function finishGame({ winner, loser }) {
	return {
		type: FINISH_GAME,
		winner,
		loser,
	};
}
