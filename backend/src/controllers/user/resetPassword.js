import crypto from 'crypto';
import User from '../../schemes/user';

import sendEmail from '../../utils/sendEmail';
import { HOST } from '../../utils/constants';

function resetPassword(req, res, next) {
    const { email } = req.body;

    User.findOne(
        {
            email
        },
        (error, user) => {
            if (error) {
                res.json({
                    success: false,
                    message: error
                });
            }

            if (!user) {
                res.json({
                    success: false
                });
            } else if (user) {
                const hash = crypto
                    .createHash('md5')
                    .update(Math.random().toString(32))
                    .digest('hex');
                const html = `
					<h1>We received a request to change your password</h1>

					<h3>Hi ${user.username},</h3>
					<p>Use the link below to set up a new password for your account. This link will expire in 4 hours.</p>

					<a href="${HOST}/reset-password/?token=${hash}">Change Password</a>
					<br />
					If you did not make this request, you do not need to do anything.
					<br />
					<br />
					Thanks for your time,
					<br />
					Spark2Date Team
				`;

                sendEmail(email, 'Spark2Data: Reset a password', html)
                    .then(() => {
                        user.resetPasswordDate = Date.now();
                        user.resetPasswordToken = hash;

                        user.save();

                        res.json({
                            success: true,
                            message: 'A reset link was sent on your email'
                        });
                    })
                    .catch(mailError => {
                        res.json({
                            success: false,
                            message: mailError
                        });
                    });
            }
        }
    );
}

export default resetPassword;
