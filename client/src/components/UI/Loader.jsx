import React from 'react';

const Loader = ({ inner }) => (
	<div className="Loader">
		{inner}
		<i className="loading" />
	</div>
);

export default Loader;
