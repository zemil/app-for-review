import React from 'react';
import styled from '@emotion/styled';
// https://material-ui.com/api/fab/
import Fab from '@material-ui/core/Fab';

const StyledFab = styled(Fab)`
	background-color: ${props => props.color === 'primary' && '#fa5596'};
	background: ${props =>
		props.color === 'primary' &&
		'linear-gradient(141deg, #c624a4 0%, #fb7e71 51%, #fa5795 75%)'};
	color: ${props => props.color === 'primary' && '#fee5e5'};

	&:hover {
		color: ${props => (props.color === 'primary' ? '#fff' : 'inherit')};
		transition: 0.3s;
	}
`;

function FabUI({ children, ...props }) {
	return <StyledFab {...props}>{children}</StyledFab>;
}

export default FabUI;
