import React from 'react';
import styled from '@emotion/styled';

const Close = styled.button`
	position: relative;
	width: ${props => props.size || 40}px;
	height: ${props => props.size || 40}px;
	border-radius: 100%;
	border: 0;
	background: ${props => props.background || 'transparent'};
	cursor: pointer;

	&:before,
	&:after {
		position: absolute;
		top: 25%;
		left: 50%;
		content: '';
		height: 50%;
		width: 2px;
		background-color: ${props => props.color || '#fff'};
	}

	&:before {
		transform: rotate(45deg);
	}

	&:after {
		transform: rotate(-45deg);
	}
`;

function CloseIco({ ...props }) {
	return <Close className="close-btn" {...props} />;
}

export default CloseIco;
