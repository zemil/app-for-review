import User from '../../schemes/user';

function createNewUser(req, res, next) {
    try {
        const { username, email, password } = req.body;

        const newUser = new User({
            username,
            email,
            password
        });

        // save the sample user
        newUser.save(function(err) {
            if (err) {
                res.json({ success: false });

                throw `createUser error: ${err}`;
            }

            res.json({ success: true });
        });
    } catch (err) {
        res.json({ success: false });

        throw `createUser error: ${err}`;
    }
}

export default createNewUser;
