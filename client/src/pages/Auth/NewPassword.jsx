import React, { Component } from 'react';

import api from '../../utils/api';
import helpers from '../../utils/helpers';

import AuthLayout from './Layout';
import Fab from '../../components/UI/Fab';
import TextField from '../../components/UI/TextField';

class AuthNewPassword extends Component {
	constructor(props) {
		super(props);

		this.state = {
			newPassword: '',
			confirmPassword: '',
			invalidFields: [],
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
	}

	handleChange(e) {
		const { name, value } = e.target;

		this.setState({
			[name]: value,
		});
	}

	get isValidForm() {
		const { newPassword, confirmPassword } = this.state;
		const invalidFields = [];

		if (newPassword.length < 8) {
			invalidFields.push('newPassword');
		}

		if (newPassword !== confirmPassword) {
			invalidFields.push('confirmPassword');
		}

		this.setState({
			invalidFields,
		});

		return invalidFields.length === 0;
	}

	handleSubmitLogin(e) {
		e.preventDefault();

		const { newPassword } = this.state;
		const token = helpers.getURLParam('token');

		if (this.isValidForm) {
			const data = {
				password: newPassword,
				token,
			};

			api.post('new-password', data)
				.then(data => data.json())
				.then(json => {
					if (json.success) {
						window.location.href = '/login';
					} else {
						alert('Problem with password changing');
					}
				})
				.catch(err => console.error('log in error: ', err));
		}
	}

	render() {
		const { newPassword, confirmPassword, invalidFields } = this.state;

		return (
			<AuthLayout onSubmit={this.handleSubmitLogin}>
				<TextField
					error={invalidFields.includes('newPassword')}
					label="New password"
					type="password"
					name="newPassword"
					margin="normal"
					variant="outlined"
					value={newPassword}
					onChange={this.handleChange}
				/>

				<TextField
					error={invalidFields.includes('confirmPassword')}
					label="Confirm password"
					type="password"
					name="confirmPassword"
					margin="normal"
					variant="outlined"
					value={confirmPassword}
					onChange={this.handleChange}
				/>

				<Fab variant="extended" color="primary" type="submit">
					Change password
				</Fab>
			</AuthLayout>
		);
	}
}

export default AuthNewPassword;
