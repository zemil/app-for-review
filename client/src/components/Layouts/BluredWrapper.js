import styled from '@emotion/styled';

const BluredWrapper = styled.div`
	width: 100%;
	height: 100%;
	background: rgba(179, 17, 179, 0.25);
	filter: blur(50px);
`;

export default BluredWrapper;
