import jwt from 'jsonwebtoken';

import { JWT_SECRET } from '../constants';

export default function decodeJWT(req) {
    const token =
        req.body.token || req.query.token || req.headers['x-access-token'];

    return jwt.decode(token, JWT_SECRET, { complete: true });
}
