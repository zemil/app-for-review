<h1 align="center">
  Spark2Date
</h1>

## Introduction

A dating video game available on all modern devices that support the web.

## Development

### Usage

To check all commands of `npm` just type `npm run`.

For development, run `npm start` to host a server of the local build. To build a production ready version run `npm run build`.

### Docker repository

This is the easiest, scalable way to deploy a build, or when you require a generic testing environnment. This requires a docker installation and quite some space (over 1 GB).

```
docker build -t spark2date:latest .
docker tag spark2date:latest 547406621487.dkr.ecr.eu-central-1.amazonaws.com/spark2date:latest
docker push 547406621487.dkr.ecr.eu-central-1.amazonaws.com/spark2date:latest
docker pull 547406621487.dkr.ecr.eu-central-1.amazonaws.com/spark2date:latest
docker run -t 547406621487.dkr.ecr.eu-central-1.amazonaws.com/spark2date:latest

```

### Deploy to Github Pages

To deploy on https://spark2date.github.io, run npm deploy.

### Mongo Database:

The client depends on `mongoose` library that handles the schema and flow to the mongo database. Currently, the endpoint for mongo is `ds255754.mlab.com:55754` with 500 free MB offered.

To connect with `mongo` run:

```
mongo <endpoint>
```

To view and analyse the data better, use [MongoDB Compass](https://www.mongodb.com/products/compass). This method allows you to see the distribution of data in many ways, including geolocation.

### Video chat

The current implementation connects a WebSocket relay server for peer to peer session initiation. Note that WebRTC video doesn't work without HTTPS!

### Deploy a build for remote testing

So far Netlify has been very unreliable, not a good option for automation. Should switch later to Jenkins, award winning automation server.

Current workaround is running `npm run build && npm run serve`

as for local testing, run `npm run develop`

## DOCS:

-   [WebRTC api](https://docs.simplewebrtc.com)
-   [Material UI](https://material-ui.com) - Components/UI based on material lib
-   [Material Icons](https://material.io/tools/icons/) - Icons list
