import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';

const userScheme = new Schema(
    {
        username: {
            type: String,
            unique: true,
            required: true
        },
        email: {
            type: String,
            unique: true,
            required: true
        },
        magic: {
            type: String
        },
        password: {
            type: String
        },
        resetPasswordToken: {
            type: String,
            default: ''
        },
        resetPasswordDate: {
            type: Date,
            default: ''
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        name: {
            type: String,
            default: ''
        },
        // age: {
        //     type: Number,
        //     default: 0
        // },
        birthday: {
            type: String,
            default: '2000-01-01'
        },
        gender: {
            type: String,
            enum: ['male', 'female']
        },
        opponentGender: {
            type: String,
            enum: ['male', 'female', 'both']
        },
        opponentAgeFrom: {
            type: Number,
            default: 0
        },
        opponentAgeTo: {
            type: Number,
            default: 0
        }
    },
    {
        collection: 'users'
    }
);

userScheme.virtual('age').get(function() {
    const birthdayYear = new Date(this.birthday).getFullYear();
    const nowYear = new Date().getFullYear();

    return nowYear - birthdayYear;
});

userScheme.methods.comparePassword = function comparePassword(
    password,
    callback
) {
    bcrypt.compare(password, this.password, callback);
};

/**
 * The pre-save hook method.
 */
userScheme.pre('save', function saveHook(next) {
    const user = this;

    // proceed further only if the password is modified or the user is new
    if (!user.isModified('password')) return next();

    return bcrypt.genSalt(10, function(saltError, salt) {
        if (saltError) return next(saltError);

        return bcrypt.hash(user.password, salt, function(hashError, hash) {
            if (hashError) {
                return next(hashError);
            }

            // replace a password string with hash value
            user.password = hash;
            user.magic = salt;

            return next();
        });
    });
});

export default mongoose.model('User', userScheme);
